package com.jkframework.training;

public enum Number {
    ONE(1),TOW(2),THREE(3),FOUR(4),FIVE(5),
    SIX(6),SEVEN(7),EIGHT(8),NINE(9),TEN(10);


    private final int numberValue;

    private Number(int numberValue){
        this.numberValue = numberValue;
    }

    public int getNumberValue(){
        return numberValue;
    }
}
