package com.jkframework.training;

public class Crad {

    // return int
    private Number number;
    // return String
    private CardType cardType;


    public Crad(Number number, CardType cardType){
        this.number = number;
        this.cardType =cardType;
    }


    public String getCardType(){
        return cardType.showCardType();
    }
    public int getNumberOfCard(){
        return number.getNumberValue();
    }

    public String toString(){
        String str = " ";
        str +=number.getNumberValue() + " of "+cardType.showCardType();
        return str;
    }
}
