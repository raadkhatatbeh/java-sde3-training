package com.jkframework.training;

import java.util.ArrayList;

public class Player {
    protected ArrayList<Crad> crads;
    private String name;
    private int score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Player(){
        crads = new ArrayList<Crad>();

    }
    public void add(Crad crad){
        crads.add(crad);
    }
    public void clear(){
        crads.clear();
    }
    public boolean give(Crad crad, Player player){
        if (!crads.contains(crad)){
            return false;
        }
        else {
            crads.remove(crad);
            player.add(crad);
            return true;
        }
    }
    public String showHandPlayer(){
        String str = "";
        System.out.println(this.getName()+" hava a :");
        for (Crad c : crads){

            str += c.toString();
            str+="\n";
        }
        return str;
    }
}
