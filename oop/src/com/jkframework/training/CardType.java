package com.jkframework.training;

public enum CardType {

    HEARTS("Hearts"),
    SPADES("Spades"),
    DIAMONDS("Diamonds"),
    CLUBS("Clubs");

    private final String cardText;

    private CardType(String cardText){
        this.cardText =cardText;
    }

    public String showCardType(){
        return cardText;
    }

}
