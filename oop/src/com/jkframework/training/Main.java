package com.jkframework.training;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Player p1,p2,p3,p4;
        Deck d1 = new Deck();
        d1.populate();
      d1.shuffle();

      p1 = new Player();
      p2 = new Player();
      p3 = new Player();
      p4 = new Player();
      Player[] players = {p1,p2,p3,p4};

        System.out.println("Hi please enter the Game name  ");
        String gameName = scanner.next();

      for (int a =0; a<players.length;a++){
          System.out.println("please enter the name of player  "+(a+1));
          String name = scanner.next();
          players[a].setName(name);
      }
        System.out.println("name of game : "+gameName);
        for (int i = 0 ; i<players.length;i++){
            System.out.println("name of player "+(i+1) +" :"+ players[i].getName());
        }

      d1.deal(players,6);

      for (int i = 0; i<players.length;i++){
          System.out.println(players[i].showHandPlayer());
      }
    }
}